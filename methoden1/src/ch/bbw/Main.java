package ch.bbw;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Geometrics geometrics = new Geometrics();

        //Übung 1.2
        double rechtecksFlaeche = geometrics.berechneRechtecksFlaeche( 10, 20.5);
        System.out.println("Rechtecksfläche: " + rechtecksFlaeche);

        //Übung 1.3
        double kreisFlaeche = geometrics.berechneKreisFlaeche(10);
        System.out.println("Kreisfläche: " + kreisFlaeche);

        //Übung 1.4
        double kreisUmfang = geometrics.berechneKreisUmfang(10);
        System.out.println("Kreisumfang: " + kreisUmfang);

        //Übung 1.5
        double zylinderOberflaeche = geometrics.berechneZylinderFlaeche(10);
        System.out.println("Zylinderoberfläche: " + zylinderOberflaeche);
    }
}
