package ch.bbw;

public class Geometrics {
    public double berechneRechtecksFlaeche(double breite, double hoehe) {
        return hoehe * breite;
    }

    public double berechneKreisFlaeche(double radius){
        return Math.PI * Math.pow(radius, 2);
    }

    public double berechneKreisUmfang(double radius){
        return 2 * Math.PI * radius;
    }

    public double berechneZylinderFlaeche(double radius, double hoehe){
        double stirnFlaeche = berechneKreisFlaeche(radius);
        double mantelLaenge = berechneKreisUmfang(radius);
        double mantelFlaeche = berechneRechtecksFlaeche(mantelLaenge, hoehe);

        return 2 * stirnFlaeche + mantelFlaeche;
    }
}